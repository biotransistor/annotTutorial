###
# title: sedHack.py
#
# author: bue
# date: 2018-05-05
# license: GPLv3
#
# input:
#     my.sed file in the ./acaxis directory
#     my.sed should conatin s///g regex
#
# run:
#     python3 sedHack.py
#
# description:
#     hack that runs the sed command specified in my.sed on every acpipeTemplateCode_*.py file
###

# load libery
import glob
import os
import sys

print('\nprocess runSets with sed.')
for s_path in {"./acaxis/","./superset/","./runset/"}:
    os.chdir(s_path)
    ls_file = glob.glob('./acpipeTemplateCode_*.py')
    for s_file in ls_file:
        print('process:', s_file )
        os.system(f"sed -f my.sed {s_file} > sed.tmp")
        os.system(f"mv sed.tmp {s_file}")
    os.chdir("..")

# return
print('ok')
