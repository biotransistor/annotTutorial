###
# title: acpipeTemplateCode_ds-Dmso1v1.py
#
# date: 2018-02-21
# license: GPL>=3
# author: bue
#
# description:
#   acpipe script to generate an perturbationset related acjson file.
#   template automatically generated by annot softawre.
#   check out: https://gitlab.com/biotransistor/annot
###

# python library
import copy
import json

# acpipe library
# check out: https://gitlab.com/biotransistor/acpipe_acjson
import acpipe_acjson.acjson as ac

# build acjson
d_acjson = ac.acbuild(
    s_welllayout='1x1',
    s_runid='ds-Dmso1v1',
    s_runtype='annot_acaxis'
)

# reagent: compound-DMSO_chebi28262-notavailable_notavailable_notavailable
s_gent = 'DMSO_chebi28262'
d_record = {s_gent: copy.deepcopy(ac.d_RECORDLONG)}
d_record[s_gent].update(copy.deepcopy(ac.d_SET))
d_record[s_gent].update(copy.deepcopy(ac.d_EXTERNALID))
d_record[s_gent].update({'manufacture': 'not_available'})
d_record[s_gent].update({'catalogNu': 'not_available'})
d_record[s_gent].update({'batch': 'not_available'})
d_record[s_gent].update({'conc': 0.032})
d_record[s_gent].update({'concUnit': 'volume_percent_uo0000165'})
d_record[s_gent].update({'cultType': 'batch'})
d_record[s_gent].update({'timeBegin': 18})
d_record[s_gent].update({'timeEnd': 90})
d_record[s_gent].update({'timeUnit': 'hour_uo0000032'})
d_record[s_gent].update({'recordSet': 'ds-Dmso1v1'})
d_record[s_gent].update({'externalId': 'LSM-36361'})
d_acjson = ac.acfuseaxisrecord(
    d_acjson,
    s_coor='1',
    s_axis='perturbation',
    d_record=d_record
)

# write to json file
ac.recordsetset(d_acjson)
with open(d_acjson['acid'], 'w') as f_acjson:
    json.dump(d_acjson, f_acjson, sort_keys=True)
