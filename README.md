![annot_tutorial_logo](img/annottutorial_logo00.png)


# Annot Tutorial

Annot and information about what Annot is can be found at  https://gitlab.com/biotransistor/annot . 
This here is just tutorial material for the tutorial described at  http://annot.readthedocs.io/en/latest/man_tutorial.html .



## About the logo

The AnnotTutorial logo is a fusion from

+ the original Annot logo, designed 2014 by Elmar Bucher
+ the Book and the Pen icon, from the [Gnome Project](https://github.com/GNOME) adwaita icon theme
